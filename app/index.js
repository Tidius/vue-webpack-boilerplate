import Vue from 'vue';
import VueRouter from 'vue-router';

import App from '@/components/app.vue';
import StartPage from '@/pages/index.vue';
import AboutPage from '@/pages/about.vue';
import '@/scss/app.scss';

// How to require folders ->
//
// const fileContextProductImages = require.context('./../assets/images/products/', false, /\.(png|jpg)$/);
// const productImagePaths = {};
//
// fileContextProductImages.keys().forEach((file) => {
//   const filename = file.replace('./', '').replace(/\.(png|jpg)$/, '');
//   productImagePaths[filename] = fileContextProductImages(file);
// });

Vue.use(VueRouter);

const routes = [
  { path: '/', component: StartPage },
  { path: '/about', component: AboutPage },
];

const router = new VueRouter({ routes, mode: 'history' });

// Setup Vue instance
const vue = new Vue({
  router,
  el: '#app',
  render: h => h(App),
});

vue.$forceUpdate();
