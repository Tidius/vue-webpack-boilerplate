import { shallowMount, RouterLinkStub } from '@vue/test-utils';
import Navigation from '@/components/navigation.vue';

describe('Navigation', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(Navigation, {
      stubs: {
        RouterLink: RouterLinkStub,
      },
    });
  });

  test('has a nav element', () => {
    expect(wrapper.contains('nav')).toBeTruthy();
  });
});
