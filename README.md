# README #

## Vue Webpack Boilerplate Project

A custom vue project boilerplate, build and compiled with webpack. For everyone who does not want to use vue cli for whatever reason. Feel free to change and customize it to your needs.

Personally I use this as a starting point for new projects, since I don't want to use cli tools like vue-cli because I like to understand whats going on in my project and how the build setup works. 


### Preconfigured features:
- Vue with vue-router
- Webpack build configuration
- ES6 transpilation with babel-preset-env
- SCSS support
- ESlint with airbnb preset
- Stylelint with airbnb preset
- Jest setup with vue-test-utils
- Bitbucket pipeline integration
- CI Deployment example for now.sh
